from aiohttp import web


def get_routes(api):
    return [
        web.get('/get_mods_by_category', api.get_mods_by_category),
        web.get('/get_mods', api.get_mods),
        web.get('/get_ratings', api.get_ratings),
        web.get('/get_comments', api.get_comments),
        web.get('/get_common_info', api.get_common_info),
        web.get('/get_mod_by_slug', api.get_mod_by_slug),
        web.get('/mod_downloads_count_inc', api.mod_downloads_count_inc),
    ]
