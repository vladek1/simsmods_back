import asyncio
import locale
import math
from collections import OrderedDict

from aiohttp import web

from api import config
from api.common import bot_user_agents
from api.db import DataBase
from api.error_messages import ERROR_MESSAGES
from api.exceptions import CategoryNotFound, ModNotFound

locale.setlocale(locale.LC_ALL, ('RU', 'UTF8'))


class Api:
    def __init__(self):
        self.app = None
        self.db = None
        self.stats = OrderedDict({
            'Модов загружено': 0,
            'Пользователей': 0,
            'Категорий': 0,
            'Скачиваний модов': 0
        })
        self.popular_mods = []
        self.site_team = []

    async def init_connection(self, app):
        self.db = DataBase()
        self.app = app
        await self.db.initialize()

    async def get_mods_by_category(self, request):
        try:
            category_name = request.query['category_name']
        except KeyError:
            return self._json_response(ERROR_MESSAGES['category_name_not_in_query'])
        page = request.query.getone('page', 1)
        page = int(page)

        try:
            lft, rght = await self.db.get_lft_rght_category(category_name)
        except CategoryNotFound:
            return self._json_response(ERROR_MESSAGES['category_not_found'])

        return await self._get_mods_by_lft_rght(lft, rght, page)

    async def get_mods(self, request):
        page = request.query.getone('page', 1)
        page = int(page)
        lft, rght = await self.db.get_lft_rght_by_root_categories()

        return await self._get_mods_by_lft_rght(lft, rght, page)

    async def get_ratings(self, request):
        try:
            mod_ids = request.query['mod_ids']
            mod_ids = mod_ids.split(',')
            if len(mod_ids) > config.MAX_MODS_FOR_GET_RATING:
                return self._json_response(ERROR_MESSAGES['mod_rating_mods_limit'])
        except KeyError:
            return self._json_response(ERROR_MESSAGES['mod_ids_not_in_query'])

        mod_ratings = await self.db.get_rating_mods(mod_ids)
        return self._json_response({
            'result': mod_ratings
        })

    async def get_comments(self, request):
        try:
            mod_id = int(request.query['mod_id'])
        except KeyError:
            return self._json_response(ERROR_MESSAGES['mod_id_not_in_query'])

        comments = await self.db.get_mod_comments(mod_id)
        return self._json_response({
            'result': comments
        })

    async def get_common_info(self, request):
        print(await self.db.get_popular_mods(config.POPULAR_MODS_COUNT))

        return self._json_response({
            'result': {
                'stats': self.stats,
                'popular_mods': self.popular_mods,
                'site_team': self.site_team
            }
        })

    async def get_mod_by_slug(self, request):
        try:
            slug = request.query['slug']
            mod = await self.db.get_mod_by_slug(slug)
        except KeyError:
            return self._json_response(ERROR_MESSAGES['slug_not_in_query'])
        except ModNotFound:
            return self._json_response(ERROR_MESSAGES['mod_not_found_by_slug'])

        self.app.loop.create_task(self._mod_view_count_inc(request.headers['User-Agent'], mod['id']))
        comments = await self.db.get_mod_comments(mod['id'])
        rating = await self.db.get_rating_mods([mod['id']])[mod['id']]

        return self._json_response({
            'result': {
                'mod': mod,
                'comments': comments,
                'rating': rating
            }
        })

    async def mod_downloads_count_inc(self, request):
        try:
            mod_id = request.query['mod_id']
        except KeyError:
            return self._json_response(ERROR_MESSAGES['mod_id_not_in_query'])

        await self.db.mod_downloads_count_inc(mod_id)
        return self._json_response({
            'result': 'success'
        })

    async def _update_common_info(self):
        while True:
            await self._update_stats()
            await self._update_popular_mods()
            await self._update_site_team()
            await asyncio.sleep(config.UPDATE_TIMEOUT)

    async def _update_stats(self):
        lft, rght = await self.db.get_lft_rght_by_root_categories()
        mods_count = await self.db.get_mods_count_by_category(lft, rght)

        users_count = await self.db.get_users_count()

        categories_count = await self.db.get_categories_count()

        downloads_count = await self.db.get_downloads_count()

        self.stats.update({
            'Модов загружено': mods_count,
            'Пользователей': users_count,
            'Категорий': categories_count,
            'Скачиваний модов': downloads_count
        })

    async def _update_popular_mods(self):
        self.popular_mods = await self.db.get_popular_mods(config.POPULAR_MODS_COUNT)

    async def _update_site_team(self):
        self.site_team = await self.db.get_site_team()

    async def _get_mods_by_lft_rght(self, lft, rght, page):
        mods_on_page = await self.db.get_mods(lft, rght, page)
        mods_count = await self.db.get_mods_count_by_category(lft, rght)
        last_page = self._last_mods_page(mods_count)
        pages = self._get_pages_for_pagination(page, last_page)
        mod_ratings = await self.db.get_rating_mods(list(mod['id'] for mod in mods_on_page))

        return self._json_response({
            'result': {
                'mods': mods_on_page,
                'pages': {
                    'current_page': int(page),
                    'pages': pages,
                    'last_page': last_page
                },
                'mod_ratings': mod_ratings
            }
        })

    def _get_pages_for_pagination(self, current_page, last_page):
        if current_page > last_page:
            return self._get_last_pages(last_page)
        counter = config.MAX_PAGES_IN_PAGINATOR
        result = [current_page]

        i = 1
        while len(result) < config.MAX_PAGES_IN_PAGINATOR and counter != 0 and \
                (current_page - i > 0 or current_page + i < last_page + 1):
            if current_page - i > 0:
                result.append(current_page - i)
                counter -= 1

            if current_page + i < last_page + 1:
                result.append(current_page + i)
                counter -= 1
            i += 1

        return sorted(result)

    async def start_background_tasks(self, app):
        app['info_updater'] = app.loop.create_task(self._update_common_info())

    async def _mod_view_count_inc(self, user_agent, mod_id):
        if user_agent not in bot_user_agents:
            await self.db.mod_views_count_inc(mod_id)

    @staticmethod
    async def cleanup_background_tasks(app):
        app['info_updater'].cancel()

    @staticmethod
    def _json_response(message):
        return web.json_response(message,
                                 headers={
                                     'Access-Control-Allow-Origin': '*',
                                 })

    @staticmethod
    def _get_last_pages(last_page):
        return list(
            filter(
                lambda x: x > 0,
                [i + 1 for i in range(last_page - config.MAX_PAGES_IN_PAGINATOR, last_page)]
            )
        )

    @staticmethod
    def _last_mods_page(mods_count):
        return math.ceil(mods_count / config.MODS_ON_PAGE)
