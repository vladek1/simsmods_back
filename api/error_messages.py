from api import config

ERROR_MESSAGES = {
    'category_name_not_in_query': {
        'error': 'Category name must have in query string'
    },
    'category_not_found': {
        'error': 'Category name is not found in Data Base'
    },
    'mod_ids_not_in_query': {
        'error': 'mod_ids is required argument in get request'
    },
    'mod_rating_mods_limit': {
        'error': 'You can not get a raring of more than %d mods' % config.MAX_MODS_FOR_GET_RATING
    },
    'mod_id_not_in_query': {
        'error': 'mod_id is required arguments for this request'
    },
    'slug_not_in_query': {
        'error': 'slug is required argument for this request'
    },
    'mod_not_found_by_slug': {
        'error': 'Mod not found by this slug'
    }
}
