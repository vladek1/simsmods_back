LFT_RGHT_BY_CATEGORY_NAME = "select lft, rght from main_category where name=%s"
MODS_INFO_BY_PAGE = "select mm.id, mm.name, mm.description, au.username, mm.published_date, mm.views_count, mm.downloads_count, mm.img, mm.mod, mm.slug from main_mod mm inner join main_category mc on mm.parent_cat_id=mc.id inner join auth_user au on au.id=mm.owner_id where mm.is_offer = 0 and mc.lft >= %s and rght <= %s order by mm.published_date DESC limit %s offset %s"
MODS_COUNT_BY_CATEGORY = "select count(*) from main_mod mm inner join main_category mc on mm.parent_cat_id=mc.id where mm.is_offer = 0 and mc.lft >= %s and rght <= %s order by mm.published_date DESC"

LFT_RGHT_BY_ROOT_CATEGORIES = "select min(mc.lft), max(mc.rght) from main_category mc where level=0"

MODS_RATING = "select parent_mod_id, round(avg(rating)) from main_modrating where parent_mod_id in %s group by parent_mod_id"

MOD_COMMENTS = "select mc.id, mc.published_date, mc.comment_text, au.username from main_comment mc inner join auth_user au on au.id=mc.parent_user_id where parent_mod_id=%s order by mc.published_date"

USERS_COUNT = "select count(*) from auth_user"
CATEGORIES_COUNT = "select count(*) from main_category"
DOWNLOADS_COUNT = "select sum(mm.downloads_count) from main_mod mm"

POPULAR_MODS = "select mm.id, mm.name, mm.description, au.username, mm.published_date, mm.views_count, mm.downloads_count from main_mod mm inner join auth_user au on au.id=mm.owner_id where mm.is_offer = 0 order by mm.downloads_count DESC limit %s"

SITE_TEAM = "select au.username, up.avatar, sum(agp.permission_id) from auth_user au inner join auth_user_groups aug on aug.user_id=au.id inner join auth_group_permissions agp on agp.group_id=aug.group_id inner join main_userprofile up on up.user_id=au.id where au.is_staff=1 group by au.id order by sum(agp.permission_id) DESC"

MOD_INFO_BY_SLUG = "select mm.id, mm.name, mm.description, au.username, mm.published_date, mm.views_count, mm.downloads_count, mm.img, mm.mod, mm.slug from main_mod mm inner join main_category mc on mm.parent_cat_id=mc.id inner join auth_user au on au.id=mm.owner_id where mm.slug=%s"

MOD_VIEWS_COUNT_INC = "update main_mod mm set mm.views_count=mm.views_count+1 where mm.id=%s"
MOD_DOWNLOADS_COUNT_INC = "update main_mod mm set mm.downloads_count=mm.downloads_count+1 where mm.id=%s"
