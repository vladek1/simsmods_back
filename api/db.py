import aiomysql

from api import query, config
from api.config import DATABASE
from api.exceptions import CategoryNotFound, ModNotFound


class DataBase:
    def __init__(self):
        self.pool = None

    async def initialize(self):
        self.pool = await aiomysql.create_pool(maxsize=30, host=DATABASE['HOST'], port=DATABASE['PORT'],
                                               user=DATABASE['USER'], password=DATABASE['PASSWORD'],
                                               db=DATABASE['NAME'], charset='utf8', autocommit=True)

    async def get_mod_comments(self, mod_id):
        db_result = await self._fetch_all(query.MOD_COMMENTS, mod_id)
        return [
            {
                'datetime': db_res[1].strftime('%d %B %Y г. %H:%M'),
                'text': db_res[2],
                'username': db_res[3]
            }
            for db_res in db_result
        ]

    async def get_rating_mods(self, mod_ids):
        db_result = await self._fetch_all(query.MODS_RATING, set(mod_ids))
        result = {k: 0 for k in mod_ids}
        for db_res in db_result:
            result.update({
                str(db_res[0]): int(db_res[1])
            })
        return result

    async def get_lft_rght_category(self, category_name):
        db_result = await self._fetch_one(query.LFT_RGHT_BY_CATEGORY_NAME, category_name)
        self.check_db_result(db_result, exception=CategoryNotFound())
        return int(db_result[0]), int(db_result[1])

    async def get_mods(self, lft, rght, page):
        db_result = await self._fetch_all(query.MODS_INFO_BY_PAGE, (lft, rght, config.MODS_ON_PAGE,
                                                                    config.MODS_ON_PAGE * (page - 1)))

        return self.get_mods_info_by_db_result(db_result)

    async def get_mods_count_by_category(self, lft, rght):
        db_result = await self._fetch_one(query.MODS_COUNT_BY_CATEGORY, (lft, rght))
        return int(db_result[0])

    async def get_lft_rght_by_root_categories(self):
        db_result = await self._fetch_one(query.LFT_RGHT_BY_ROOT_CATEGORIES)
        return int(db_result[0]), int(db_result[1])

    async def get_users_count(self):
        db_result = await self._fetch_one(query.USERS_COUNT)
        return int(db_result[0])

    async def get_categories_count(self):
        db_result = await self._fetch_one(query.CATEGORIES_COUNT)
        return int(db_result[0])

    async def get_downloads_count(self):
        db_result = await self._fetch_one(query.DOWNLOADS_COUNT)
        return int(db_result[0])

    async def get_popular_mods(self, count):
        db_result = await self._fetch_all(query.POPULAR_MODS, count)
        return self.get_mods_info_by_db_result(db_result)

    async def get_site_team(self):
        db_result = await self._fetch_all(query.SITE_TEAM)
        return [
            {
                'username': db_res[0],
                'avatar': db_res[1]
            }
            for db_res in db_result
        ]

    async def get_mod_by_slug(self, slug):
        db_result = await self._fetch_one(query.MOD_INFO_BY_SLUG, slug)
        self.check_db_result(db_result, exception=ModNotFound(reason='slug'))
        return self.get_mod_info(db_result)

    async def mod_views_count_inc(self, mod_id):
        await self._fetch_one(query.MOD_VIEWS_COUNT_INC, mod_id)

    async def mod_downloads_count_inc(self, mod_id):
        await self._fetch_one(query.MOD_DOWNLOADS_COUNT_INC, mod_id)

    async def _fetch_one(self, db_query, *args, **kwargs):
        with await self.pool as connection:
            cur = await connection.cursor()
            await cur.execute(db_query, *args, **kwargs)
            return await cur.fetchone()

    async def _fetch_all(self, db_query, *args, **kwargs):
        with await self.pool as connection:
            cur = await connection.cursor()
            await cur.execute(db_query, *args, **kwargs)
            return await cur.fetchall()

    def get_mods_info_by_db_result(self, db_result):
        return [self.get_mod_info(db_res) for db_res in db_result]

    @staticmethod
    def get_mod_info(db_result):
        return {
            'id': db_result[0],
            'name': db_result[1],
            'description': db_result[2],
            'owner': db_result[3],
            'published_date': db_result[4].strftime('%d %B %Y г. %H:%M'),
            'views_count': db_result[5],
            'downloads_count': db_result[6],
            'img': db_result[7],
            'mod': db_result[8],
            'slug': db_result[9]
        }

    @staticmethod
    def check_db_result(db_result, exception=Exception()):
        if not db_result:
            raise exception
