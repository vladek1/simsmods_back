class CategoryNotFound(Exception):
    def __str__(self):
        return 'Category not found in data base'


class ModNotFound(Exception):
    def __init__(self, reason):
        self.reason = reason

    def __str__(self):
        return f'Mod not found by {self.reason}'
