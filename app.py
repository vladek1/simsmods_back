from aiohttp import web

from api.api import Api
from api.routes import get_routes


def main():
    api = Api()

    app = web.Application()
    app.on_startup.append(api.init_connection)
    app.on_startup.append(api.start_background_tasks)
    app.on_cleanup.append(api.cleanup_background_tasks)
    app.router.add_routes(get_routes(api))

    web.run_app(app)


if __name__ == '__main__':
    main()
